//
//  InitialSignUpVC.swift
//  ShowcaseIOS
//
//  Created by Moshiur Rahman Bilash on 4/19/21.
//

import UIKit

class InitialSignUpVC: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailtextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    let toolbar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createToolbar()
        setUpViews()
    }
    
    func setUpViews() {
        self.usernameTextField.delegate = self
        self.emailtextField.delegate = self
        self.passwordTextField.delegate = self
    }
    @IBAction func signUpButtonEventListener(_ sender: Any) {
        self.performSegue(withIdentifier: "LoginViewController", sender: self)
    }
    
    @IBAction func signInButtonEventListener(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension InitialSignUpVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.text = ""
        textField.inputAccessoryView = toolbar
    }
    
    func createToolbar() {
        
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePressed))
        //        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPickerView));
        //        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
    }
    
    @objc  func donePressed() {
        self.view.endEditing(true)
    }
    @objc  func cancelPickerView() {
        self.view.endEditing(true)
    }


}

