//
//  LocationViewController.swift
//  ShowcaseIOS
//
//  Created by Moshiur Rahman Bilash on 4/19/21.
//

import UIKit

class LocationViewController: UIViewController {

    @IBOutlet weak var typesOfAreaTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    let toolbar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createToolbar()
        setUpViews()
    }
    
    func setUpViews() {
        self.typesOfAreaTextField.delegate = self
        self.cityTextField.delegate = self
    }
    
    @IBAction func backButtonEventListener(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func signUpButtonClick(_ sender: Any) {
        self.performSegue(withIdentifier: "Login", sender: self)
    }
    
}

extension LocationViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.text = ""
        textField.inputAccessoryView = toolbar
    }
    
    func createToolbar() {
        
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePressed))
        //        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPickerView));
        //        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
    }
    
    @objc  func donePressed() {
        self.view.endEditing(true)
    }
    @objc  func cancelPickerView() {
        self.view.endEditing(true)
    }


}

