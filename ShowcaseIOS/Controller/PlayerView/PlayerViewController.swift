//
//  PlayerViewController.swift
//  ShowcaseIOS
//
//  Created by Moshiur Rahman Bilash on 4/7/21.
//

import UIKit
import AVFoundation
import AVKit
import CoreMedia
import NVActivityIndicatorView


class PlayerViewController: UIViewController {
    
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    @IBOutlet var dismiss1: UIButton!
    @IBOutlet var topView: UIView!
    @IBOutlet var vlcPlayerView: UIView!
    // @IBOutlet var playerView: UIView!
    var player: VLCMediaPlayer!
    @IBOutlet var dismissButton: UIButton!
    var streamName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingView.startAnimating()
        self.vlcPlayerView.frame = UIScreen.screens[0].bounds
        
        player = VLCMediaPlayer()
        player.media = VLCMedia(url: URL(string: "rtmp://52.91.118.31/LiveApp/\(streamName)")!)
        
        //https://d2mdw063ttlqtq.cloudfront.net/h264-video-previews/80fad324-9db4-11e3-bf3d-0050569255a8/490527.mp4
        
        player.drawable = self.vlcPlayerView
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PopOverViewController")
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)

        
        if !player.isPlaying {
            let vc = PopOverViewController()
            self.present(vc, animated: true, completion: nil)
            print("oooooooooooo")
            player.play()
        }
    }
    
    
}
