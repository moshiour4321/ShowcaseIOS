//
//  PopOverViewController.swift
//  ShowcaseIOS
//
//  Created by Moshiur Rahman Bilash on 4/7/21.
//

import UIKit

class PopOverViewController: UIViewController {
    
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var commentTextField: UITextField!
    
    var comments = ["Product is too good", "I like this product", "How much price?", "How can i get this product?", "I need this product?", "I want to buy this product?", "Is it possible to discount some", "Product not too good", "I don't like this product", "Please call me 01743222222, i want to buy this product"]
    
    var commentsData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentTextField.attributedPlaceholder = NSAttributedString(string: "Write some comment", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        print("pop")
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func closeTapped(_ sender: UIButton) {
        
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        
        //self.dismiss(animated: true, completion: nil)
    }
    
}

extension PopOverViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentsTableViewCell
        
        cell.commentLabel.text = commentsData[indexPath.row]
        
        return cell
    }
    
    
}
