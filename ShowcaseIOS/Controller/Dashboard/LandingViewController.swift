//
//  LandingViewController.swift
//  SuperApp
//
//  Created by Moshiur Rahman Bilash on 22/12/20.
//

import UIKit

enum CellIdentifier: String {
    case landingCollectionViewCellID = "landingCollectionView"
    case landingTableViewCellID = "cell"
    case categoryTableViewCellID = "categoryCell"
}


class LandingViewController: UIViewController {
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet var categoryTableView: UITableView!
    @IBOutlet var categoryTableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var CategoryListContainerView: UIView!
    @IBOutlet var categoryListWidthConstraint: NSLayoutConstraint!
    @IBOutlet var categoryListHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var navigationItemContainerView: UIView!
    @IBOutlet var profileMenuContainerView: UIView!
    @IBOutlet var menuContainerView: UIView!
    //@IBOutlet var profileMenuView: CardView!
    @IBOutlet var addToFavoriteButton: UIButton!
    @IBOutlet var productDetailsButton: UIButton!
    //@IBOutlet var menuView: CardView!
    @IBOutlet var updateProfileButton: UIButton!
    @IBOutlet var changePasswordButton: UIButton!
    @IBOutlet var registeredDeviceButton: UIButton!
    @IBOutlet var logoutButton: UIButton!
    @IBOutlet var profileMenuContainerViewTopConstraint: NSLayoutConstraint!
    
    let itemImageArray = [UIImage(named: "item1"), UIImage(named: "item2"),UIImage(named: "item3"),UIImage(named: "item4"),UIImage(named: "item5"),UIImage(named: "item6")]
    let toolbar = UIToolbar()
    
    var pageControl: UIPageControl?

    var item: Int?
    var tableViewHeight: CGFloat {
        self.categoryTableView.layoutIfNeeded()
        
        return categoryTableView.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        
    }
    
    func setUpView() {
        
        //profileImageView.layer.cornerRadius = 14
        
//        updateProfileButton.isExclusiveTouch = true
//        changePasswordButton.isExclusiveTouch = true
//        registeredDeviceButton.isExclusiveTouch = true
//        logoutButton.isExclusiveTouch = true
        createToolbar()
        loadCollectionView()
        self.searchField.delegate = self

//        if let assetResolution = getAssetResolution() {
//            viewModel.deviceResulation = assetResolution
//        } else {
//            viewModel.deviceResulation = "2x"
//        }
        
    }
    
    @IBAction func dummyItem1Event(_ sender: Any) {
        self.performSegue(withIdentifier: "showLive", sender: self)
    }
    
    @IBAction func leftmenuButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dummyItem2Event(_ sender: Any) {
        self.performSegue(withIdentifier: "showLive", sender: self)
    }
    
    override func updateViewConstraints() {
        
        //categoryListHeightConstraint.constant = tableViewHeight
        super.updateViewConstraints()
    }
    
    func getAssetResolution() -> String? {
        var assetResolution: String?
        let assetType = UIScreen.main.scale
        switch assetType {
        case 1.0:
            assetResolution = "1x"
            break
        case 2.0:
            assetResolution = "2x"
            break
        case 3.0:
            assetResolution = "3x"
            break
        default:
            assetResolution = "2x"
        }
        
        return assetResolution
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        print("viewDidDisappear called")
        //viewModel.viewDidDisappear()
    }
    
    @IBAction func categoryButtonEventListener(_ sender: Any) {
        
        CategoryListContainerView.frame.size.height = self.view.frame.size.height
        CategoryListContainerView.frame.size.width = self.view.frame.size.width
        self.CategoryListContainerView.isHidden = false
        self.view.addSubview(CategoryListContainerView)
        
        
        self.categoryListHeightConstraint.constant = tableViewHeight
        updateViewConstraints()
        print("tableViewHeight: \(tableViewHeight)")
        categoryTableViewTopConstraint.constant = navigationItemContainerView.frame.origin.y + (navigationItemContainerView.frame.size.height - 20)
        self.categoryTableView.isScrollEnabled = false
        
        
        print("constant : \(categoryTableViewTopConstraint.constant)")
    }
    
    
    @IBAction func backButtonEventListener(_ sender: Any) {
        //viewModel.backButtonEvent()
    }
    
    @IBAction func updateProfileButtonEventListener(_ sender: Any) {
        //viewModel.hitUpdateProfileAction()
        //handlePofileContainerViewTap()
    }
    
    @IBAction func changePasswordButtonEventListener(_ sender: Any) {
        //viewModel.hitChangePasswordAction()
        //handlePofileContainerViewTap()
    }
    
    @IBAction func registeredDeviceButtonEventListener(_ sender: Any) {
        //viewModel.hitRagisterDeviceAction()
        //handlePofileContainerViewTap()
    }
    
    @IBAction func logoutButtonEventListener(_ sender: Any) {
        //handlePofileContainerViewTap()
    }
    
    
    deinit {
        print("deinnnnittt")
    }
    
}


extension LandingViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func loadCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        self.collectionView?.isScrollEnabled = true
        self.collectionView?.isPagingEnabled = true
        self.collectionView?.bounces = true
        self.collectionView?.bouncesZoom = false
        self.collectionView?.alwaysBounceVertical = false
        self.collectionView?.showsHorizontalScrollIndicator = false
        self.collectionView?.showsVerticalScrollIndicator = false
        let nibCell = UINib(nibName: "CollectionViewCell", bundle: nil)
        self.collectionView?.register(nibCell, forCellWithReuseIdentifier: CellIdentifier.landingCollectionViewCellID.rawValue)
        addPageControl()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.landingCollectionViewCellID.rawValue, for: indexPath) as! CollectionViewCell
        cell.landingImageView.image = itemImageArray[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height = collectionView.collectionViewLayout.collectionViewContentSize.height
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl?.currentPage = indexPath.item
    }
    
    func addPageControl() {
        self.pageControl = UIPageControl(frame: CGRect(x:500, y: 500, width:150, height:50))
        
        self.pageControl?.numberOfPages = self.itemImageArray.count
        self.pageControl?.currentPage = 0
        //self.pageControl?.backgroundColor = UIColor(red: 30.0/255.0, green: 51.0/255.0, blue: 109.0/255.0, alpha: 1.0)
        self.pageControl?.pageIndicatorTintColor = UIColor(red: 196.0/255.0, green: 196.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        self.pageControl?.currentPageIndicatorTintColor = UIColor.white
        self.pageControl?.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.pageControl?.widthAnchor.constraint(equalToConstant: 150).isActive = true
        self.pageControl?.layer.cornerRadius = 10
        self.pageControl?.isUserInteractionEnabled = false
        self.view?.addSubview(self.pageControl.unsafelyUnwrapped)
        self.pageControl?.translatesAutoresizingMaskIntoConstraints = false
        
        self.pageControl?.bottomAnchor.constraint(equalTo: collectionView.unsafelyUnwrapped.bottomAnchor, constant: -0).isActive = true
        self.pageControl?.centerXAnchor.constraint(equalTo: collectionView.unsafelyUnwrapped.centerXAnchor).isActive = true
        // self.pageControl?.leftAnchor.constraint(equalTo: collectionView.unsafelyUnwrapped.leftAnchor, constant: 30).isActive = true
        
        print("222222")
        
    }
    
    
}

extension LandingViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.text = ""
        textField.inputAccessoryView = toolbar
    }
    
    func createToolbar() {
        
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePressed))
        //        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPickerView));
        //        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
    }
    
    @objc  func donePressed() {
        self.view.endEditing(true)
    }
    @objc  func cancelPickerView() {
        self.view.endEditing(true)
    }


}


