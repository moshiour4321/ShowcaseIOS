//
//  LoginViewController.swift
//  ShowcaseIOS
//
//  Created by Moshiur Rahman Bilash on 4/19/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailAddresstextField: UITextField!
    @IBOutlet weak var passwordtextField: UITextField!
    let toolbar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createToolbar()
        setUpViews()
    }
    
    func setUpViews() {
        self.emailAddresstextField.delegate = self
        passwordtextField.delegate = self
    }
    

    
    @IBAction func loginButtonEventListener(_ sender: Any) {
        self.performSegue(withIdentifier: "dashboard", sender: self)
    }
    
    @IBAction func createNewAccBtnEventListener(_ sender: Any) {
        self.performSegue(withIdentifier: "signUp", sender: self)
    }
    
}


extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.text = ""
        textField.inputAccessoryView = toolbar
    }
    
    func createToolbar() {
        
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePressed))
        //        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPickerView));
        //        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
    }
    
    @objc  func donePressed() {
        self.view.endEditing(true)
    }
    @objc  func cancelPickerView() {
        self.view.endEditing(true)
    }


}
